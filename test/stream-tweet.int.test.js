const { createServer } = require('http');
const { Server } = require('socket.io');
const Client = require('socket.io-client');
const { PassThrough } = require('stream');

const { streamTweets } = require('../src/stream-tweet');

const mockedStream = new PassThrough();

jest.mock('../src/utils/getStream', () => ({
  getStream: jest.fn(),
}));

describe('stream-tweet @mock @integration', () => {
  let io, clientSocket;

  beforeAll((done) => {
    const httpServer = createServer();
    io = new Server(httpServer);
    httpServer.listen(() => {
      const port = httpServer.address().port;
      clientSocket = new Client(`http://localhost:${port}`);
      io.on('connection', (socket) => {
        streamTweets(io, mockedStream);
      });
      clientSocket.on('connect', done);
    });
  });

  afterAll(() => {
    io.close();
    clientSocket.close();
  });

  test('should stream tweets', (done) => {
    clientSocket.on('tweet', (arg) => {
      expect(arg.key).toBe('hello world');
      done();
    });
    mockedStream.emit('data', JSON.stringify({ key: 'hello world' }));
  });
});
