const http = require('http');
const express = require('express');

const { getIO, initiateConnection } = require('../src/socket-gateway');

describe('socket gateway @mock @unit', () => {
  let server;
  let app;
  let io;
  let mockStreamTweets;

  beforeAll(() => {
    app = express();
    server = http.createServer(app);
  });

  it('should return socket instance', () => {
    io = getIO(server);
    expect(io).not.toBeNull();
  });

  it('should call streamTweets when initiate connection', () => {
    mockStreamTweets = jest.fn(() => ({
      on: ('timeout', () => {}),
    }));
    io = {
      on: () => {
        mockStreamTweets(io);
      },
      close: () => {},
    };

    initiateConnection(io, mockStreamTweets);

    expect(mockStreamTweets.mock.calls.length).toBe(1);
  });

  afterAll(() => {
    io.close();
  });
});
