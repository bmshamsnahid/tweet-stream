const http = require('http');
const path = require('path');
const express = require('express');

const logger = require('./utils/logger');
const { getIO, initiateConnection } = require('./socket-gateway');
const { streamTweets } = require('./stream-tweet');

const PORT = process.env.PORT || 3000;

const app = express();
const server = http.createServer(app);

// Render the web view to display the tweets
app.get('/', (req, res) => {
  res.sendFile(path.resolve(__dirname, '../', 'client', 'index.html'));
});

// Getting the stream of tweets
const io = getIO(server);

// Initiate the socket connection and broadcast tweets to end users
initiateConnection(io, streamTweets);

server.listen(PORT, () => logger.info(`Server started and running on Port ${PORT}`));
