const socketIo = require('socket.io');
const { getStream } = require('./utils/getStream');

/**
 * ## getIO
 *
 * Use to return the instance of socket.io
 */
const getIO = (server) => socketIo(server);

/**
 * ## initiateConnection
 *
 * Use to integrate twitter stream and broadcasting
 * First get the stream and then broadcast the tweets found from the stream
 */
const initiateConnection = (io, streamTweets) => {
  io.on('connection', async () => {
    const stream = await getStream();
    const filteredStream = streamTweets(io, stream);

    let timeout = 0;
    filteredStream.on('timeout', () => {
      // Reconnect on error
      setTimeout(() => {
        timeout += 1;
        streamTweets(io);
      }, 2 ** timeout);
      streamTweets(io);
    });
  });
};

module.exports = {
  getIO,
  initiateConnection,
};
