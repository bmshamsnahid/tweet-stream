const logger = require('./utils/logger');

/**
 * ## streamTweets
 * @param {object} socket socket instance
 * @param {object} stream twitter streamer
 *
 * @description Use to broadcast the tweets to tbe end users
 *
 */
const streamTweets = async (socket, stream) => {
  stream.on('data', (data) => {
    try {
      const json = JSON.parse(data);
      logger.info(data);
      socket.emit('tweet', json);
    } catch (error) {
      logger.error(error);
      // handle error
      throw error;
    }
  });
};

module.exports = {
  streamTweets,
};
