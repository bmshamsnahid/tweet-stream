const winston = require('winston');

// Application log configurations
// TODO: Integrate logs to database or file to determine issues in prod
const logConfiguration = {
  transports: [new winston.transports.Console()],
  format: winston.format.combine(
    winston.format.label({
      label: 'Label🏷️',
    }),
    winston.format.timestamp({
      format: 'MMM-DD-YYYY HH:mm:ss',
    }),
    winston.format.printf(
      (info) => `${info.level}: ${info.label}: ${[info.timestamp]}: ${info.message}`,
    ),
  ),
};

module.exports = winston.createLogger(logConfiguration);
