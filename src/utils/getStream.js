const needle = require('needle');
require('dotenv').config();

const TOKEN = process.env.TWITTER_BEARER_TOKEN;
const STREAM_URL = 'https://api.twitter.com/2/tweets/search/stream?tweet.fields=public_metrics&expansions=author_id';

/**
 * ## getStream
 * Use to get the stream object from twitter
 * Stream is being used to get the twitter feeds on real time
 *
 * @returns twitter stream object
 */
const getStream = () => needle.get(STREAM_URL, {
  headers: {
    Authorization: `Bearer ${TOKEN}`,
  },
});

module.exports = {
  getStream,
};
