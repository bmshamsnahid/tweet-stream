# Tweet Stream

Stream tweets to end users

## Run Local Machine

---

### With Docker

- Make sure `docker` is installed in the system
- Create a `.env` file, and put the twitter developer account token (An example is provided to `.env.example` file)
- Go to project directory and run `docker-compose up`
- Open browser and go to `http://localhost:3000` and tweeter should be streaming

### Without Docker

- Make suer in machine, `Node.js` is `14.x` or up
- Install packages by `yarn` or `npm i`
- Run app by `yarn start`

### Tests

To run tests in local machine, run `yarn test`.

### Next tasks (Not Done Yet)

- Twitter allows to select certain amount of topics (Right now 25) to choose to get the tweets. Need an crud endpoints to update these rules
- Right now any user can access these tweets, need an auth point to validate the users
- Tweets are coming from tweeter very un-predictable amounts in a single time. For some topics, sometimes 20 tweets at a time. To make better user experiences, we can set up our own streaming tool to make a consistency between our application and end users.
